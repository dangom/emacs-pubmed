# Changelog

## [Unreleased]
### Added
- Added Springer fulltext function

## [0.2] - 2019-05-01
### Added
- Support summaries of books and chapters
- Added Open Access Button fulltext function
- Added BibTeX keypattern functions
- Make BibTeX citation keys unique by adding a letter
- Writing BibTeX references appends in stead of overwrites by default

## [0.1.1] - 2019-04-28
### Added
- Now available from MELPA

## [0.1] - 2019-03-26
- Initial Release
